import pandas as pd
import sys
import os
from datetime import datetime, timedelta
from cli import ArgumentOpsi
from datedict import DateParam
from extract_script import get_data
from transform_script import get_list_timestamp, plus_seven_hour, add_database_source_name

# yesterday = datetime.now() - timedelta(1)
# year = str(yesterday.year)
# month = str(yesterday.month)
# day1 = str(yesterday.day)

# month = '0' + str(month) if len(str(month)) == 1 else month
# day1 = '0' + str(day1) if len(str(day1)) == 1 else day1

# filename = dbhost.replace('.','_') + '_' + year + month + day1

# Get Data from DB
# def get_data(dbhost, dbname, dbuser, dbpass, table, timecolumn, today):
#     conin = psycopg2.connect(   
#                                 host=dbhost, 
#                                 database=dbname, 
#                                 user=dbuser, 
#                                 password=dbpass
#                             )

#     query = """ select * 
#                 from "{}" 
#                 where "{}" + interval '7 hour' <=
#                 '{} 00:00:00'
#             """.format(table, timecolumn, today)

#     df = pd.read_sql(query, con=conin)
#     return df

# Check if there's any timestamp on the Data
# def get_list_timestamp(df):
#     check_timestamp = df.columns.groupby(df.dtypes)
#     tipe_data = ''

#     for time in check_timestamp:
#         if 'datetime' in str(time):
#             print(time)
#             tipe_data = time
#             break
#         else:
#             # print('tidak ada timestamp')
#             continue
#     list_timestamp = list(check_timestamp[tipe_data])
#     return list_timestamp

# transform to add +7 hour to Data
# def plus_seven_hour(df, list_timestamp):
#     for l in list_timestamp:
#         df[str(l)] = df[str(l)] + timedelta(hours=7)
#     return df 

# transform to add additional column to define data source
# def add_database_source_name(df, dbhost):
#     df['database'] = dbhost
#     return df


if __name__ == "__main__":
    arguments = sys.argv[1:]
    param = ArgumentOpsi(arguments)
    today, yesterday, year, month, day = DateParam()

    df = get_data(param.dbhost, param.dbname, param.dbuser, param.dbpassword, param.table, param.timecolumn, today.date())
    print(df)

    list_timestamp = get_list_timestamp(df)

    print(list_timestamp)

    df = plus_seven_hour(df, list_timestamp)

    print(df)

    df = add_database_source_name(df, param.dbhost)

    print(df)