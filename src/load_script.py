import sqlalchemy
import pandas as pd 

def load_to_table(df):
    table_name = 'test_table_2'
    df.reset_index(drop=True, inplace=True)
    
    sql_engine = sqlalchemy.create_engine("postgresql+psycopg2://qluedata:QlueJakarta1@streamdb.cyiaz047hgf7.ap-southeast-1.rds.amazonaws.com:5432/qluedata")
    sql_db = pd.io.sql.SQLDatabase(sql_engine)

    check_and_create_table(df, sql_db, table_name)
    print('Upload Data to the Database')
    df[21:50].to_sql(table_name, sql_engine, if_exists='append', index=False, chunksize=100)
    print('Data has been uploaded to database')


def check_and_create_table(df, sql_db, table_name):
    if not sql_db.has_table(table_name):
        print('No Table '+table_name+' has been found')
        print('created table')
        args = [table_name, sql_db]
        kwargs = {
            "frame" : df,
            "index" : True,
            "index_label" : "index",
            "keys" : "index"
        }
        sql_table = pd.io.sql.SQLTable(*args, **kwargs)
        sql_table.create()
    else:
        print('Table Has Been Created Before')