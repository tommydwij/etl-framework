from datetime import datetime, timedelta

def DateParam():
    today = datetime.now()
    yesterday = datetime.now() - timedelta(1)
    year = str(yesterday.year)
    month = str(yesterday.month)
    day = str(yesterday.day)
    
    month = '0' + str(month) if len(str(month)) == 1 else month
    day1 = '0' + str(day) if len(str(day)) == 1 else day

    return today, yesterday, year, month, day1
