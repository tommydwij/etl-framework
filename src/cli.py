import argparse
import sys
from config.config import CONFIG
parser = argparse.ArgumentParser()

def ArgumentOpsi(argv=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('--dbhost', help='Add Database Hostname, is Mandatory!', required=True)
    parser.add_argument('--dbport', help='Add Database Port', default=CONFIG['qv_port'], const=CONFIG['qv_port'], nargs='?')
    parser.add_argument('--dbname', help='Add Database Name, is Mandatory!', required=True)
    parser.add_argument('--dbuser', help='Add Database User', default=CONFIG['qv_username'], const=CONFIG['qv_username'], nargs='?')
    parser.add_argument('--dbpassword', help='Add Database Password', default=CONFIG['qv_password'], const=CONFIG['qv_password'], nargs='?')
    parser.add_argument('--table', help='Add Table Name to Extract, is Mandatory!', required=True)
    parser.add_argument('--timecolumn', help='Add Timestamp Column, is Mandatory!', required=True)
    ops = parser.parse_args(argv)
    return ops 