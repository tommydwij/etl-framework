import pandas as pd
from datetime import timedelta

# Check if there's any timestamp on the Data
def get_list_timestamp(df):
    check_timestamp = df.columns.groupby(df.dtypes)
    tipe_data = ''

    for time in check_timestamp:
        if 'datetime' in str(time):
            print(time)
            tipe_data = time
            break
        else:
            # print('tidak ada timestamp')
            continue
    list_timestamp = list(check_timestamp[tipe_data])
    return list_timestamp

# transform to add +7 hour to Data
def plus_seven_hour(df, list_timestamp):
    for l in list_timestamp:
        df[str(l)] = df[str(l)] + timedelta(hours=7)
    return df 

# transform to add additional column to define data source
def add_database_source_name(df, dbhost):
    df['database'] = dbhost
    return df