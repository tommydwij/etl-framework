import pandas as pd
import psycopg2

def get_data(dbhost, dbname, dbuser, dbpass, table, timecolumn, today):
    conin = psycopg2.connect(   
                                host=dbhost, 
                                database=dbname, 
                                user=dbuser, 
                                password=dbpass
                            )

    query = """ select * 
                from "{}" 
                where "{}" + interval '7 hour' <=
                '{} 00:00:00'
            """.format(table, timecolumn, today)

    df = pd.read_sql(query, con=conin)
    df['database_name'] = dbhost
    return df