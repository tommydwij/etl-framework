import os
import pandas as pd

def df_to_csv(df, save_dir, dbhost, year, month, day):
    dbhost = dbhost.replace('.','_')
    # print(dbhost)
    filename = '{}/{}_{}{}{}.csv'.format(save_dir, dbhost, year, month, day)
    # print(filename) 
    # df.to_csv(filename, index=False)
    print(df.dtypes)
    print('Data saved to : {}'.format(filename))

def check_and_create_folder(save_dir):
    print('Checking is directory available or not')
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
        print('Directory created')
    else:
        print('Directory has been created before')
