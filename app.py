# import pandas as pd
import sys
import os
from datetime import datetime, timedelta
from src.cli import ArgumentOpsi
from src.datedict import DateParam
from src.extract_script import get_data
from src.transform_script import get_list_timestamp, plus_seven_hour
from src.load_script import load_to_table

if __name__ == "__main__":
    start = datetime.now()
    print('Start at : ' + str(start))
    arguments = sys.argv[1:]
    param = ArgumentOpsi(arguments)
    today, yesterday, year, month, day = DateParam()

    # Extract Data 
    df = get_data(param.dbhost, param.dbname, param.dbuser, param.dbpassword, param.table, param.timecolumn, today.date())
    
    print(df)

    # Transform Data
    list_timestamp = get_list_timestamp(df)

    print(list_timestamp)

    df = plus_seven_hour(df, list_timestamp)

    print(df)

    # load_to_table(df)
    end = datetime.now()
    print('End at : ' + str(end))
    print('Proccess time : ' + str(end - start))


